<?php

use Illuminate\Database\Seeder;
use Novedades52\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'first_name' => 'Jose Ángel',
            'last_name' => 'Salvador Moreno',
            'email' => 'joseangelsalvador57@gmail.com',
            'role' => 'admin',
            'password' => bcrypt('secret'),
        ]);
        factory(User::class)->create([
            'role' => 'admin',
        ]);
        factory(User::class)->times(4)->create();
    }
}
