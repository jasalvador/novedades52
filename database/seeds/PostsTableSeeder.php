<?php

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;
use Novedades52\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        factory(Post::class)->times(30)->make()->each(function ($post) use ($users) {
            $post->user_id = $users->random()->id;
            $post->save();
        });
    }
}
