<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthenticationTest extends TestCase
{
    use DatabaseTransactions;

    protected $firstName = 'Usuario';
    protected $lastName = 'Prueba';
    protected $email = 'user@gmail.com';
    protected $password = 'secret';

    function test_user_can_register()
    {
        $this->visit('/')
            ->click('Register')
            ->see('Register')
            ->seePageIs('/register')
            ->type($this->firstName, 'first_name')
            ->type($this->lastName, 'last_name')
            ->type($this->email, 'email')
            ->type($this->password, 'password')
            ->type($this->password, 'password_confirmation')
            ->press('Register');

        $this->seeCredentials([
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'email' => $this->email,
            'password' => $this->password,
        ]);

        $user = \Novedades52\User::where(['email' => $this->email])->first();

        $this->seePageIs('/')
            ->see('Welcome')
            ->see($user->name);
    }

    function test_a_user_can_login()
    {
        $user = factory(\Novedades52\User::class)->create([
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'email' => $this->email,
            'password' => bcrypt($this->password),
        ]);

        $this->visit('/')
            ->click('Login')
            ->type($this->email, 'email')
            ->type($this->password, 'password')
            ->press('Login');

        $this->seeIsAuthenticated();

        $this->seeIsAuthenticatedAs($user);

        $this->seePageIs('/')
            ->see('Welcome')
            ->see($user->name);
    }

    function test_a_user_can_logout()
    {
        $user = $this->createUser();

        $this->actingAs($user)
            ->visit('/')
            ->seeLink('Logout')
            ->click('Logout');

        $this->dontSeeIsAuthenticated();

        $this->visit('/')
            ->dontSee($user->name);
    }

    function test_an_admin_can_login_as_another_user()
    {
        $admin = $this->createAdmin();
        $user = $this->createUser();

        $this->actingAs($admin)
            ->visit('/admin/login-as')
            ->seeLink('Login as')
            ->click('Login as')
            ->seePageIs('/')
            ->see($user->name)
            ->seeIsAuthenticatedAs($user);
    }

    function test_an_admin_cannot_login_as_another_admin()
    {
        $admin = $this->createAdmin();
        $anotherAdmin = $this->createAdmin();

        $this->actingAs($admin)
            ->visit('/admin/login-as/' . $anotherAdmin->id)
            ->seePageIs('/admin/login-as')
            ->see('You cannot login as an admin')
            ->seeIsAuthenticatedAs($admin);
    }

    function test_an_admin_can_login_as_another_user_and_return()
    {
        $admin = $this->createAdmin();
        $user = $this->createUser();

        $this->actingAs($admin)
            ->visit('/admin/login-as/' . $user->id)
            ->seeText($admin->name . ' logged as ' . $user->name)
            ->seeLink('Back to admin')
            ->click('Back to admin')
            ->seeIsAuthenticatedAs($admin);
    }
}
