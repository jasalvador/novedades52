<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class WelcomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_welcome_user()
    {
        $this->visit('/')
            ->seeElement('img', ['src' => 'http://novedades52.laravel.app/laravel_logo.png', 'alt'])
            ->seeText('Welcome to Styde');
    }

}
