<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminUsersTest extends TestCase
{
    use DatabaseTransactions;

    function test_list_users()
    {
        $admin = $this->createAdmin();

        $this->actingAs($admin)
            ->visit('/admin/users')
            ->see('Users')
            ->within('#content', function () use ($admin) {
                $this->see($admin->name)
                    ->see($admin->email)
                    ->seeLink('Edit');
            });
    }
}
