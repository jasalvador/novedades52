<?php

namespace Novedades52\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Novedades52\Post;
use Novedades52\User;

class PostPolicy
{
    use HandlesAuthorization;

    public function view()
    {
        return true;
    }

    public function create()
    {
        return true;
    }

    public function update(User $user, Post $post)
    {
        return $user->id == $post->user_id;
    }

    public function delete()
    {
        return true;
    }
}
