<?php

namespace Novedades52;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = ['name', 'years'];
}
