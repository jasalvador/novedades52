<?php

namespace Novedades52\Http\Controllers;

use Illuminate\Http\Request;

use Novedades52\Experience;
use Novedades52\Http\Requests;
use Novedades52\User;

class ProfileController extends Controller
{
    public function edit()
    {
        $user = User::first();

        return view('users.edit-profile', compact('user'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'experience.*.technology' => 'between:2,10',
            'experience.*.years'      => 'required_with:experience.*.technology|integer',
        ]);

        $experiences = [];

        $user = User::first();

        $user->experiences()->delete();

        foreach ($request->get('experience') as $item) {
            if (empty($item['technology']))
                continue;

            $experiences[] = new Experience([
                'name'  => $item['technology'],
                'years' => $item['years'],
            ]);
        }

        $user = User::first();

        $user->experiences()->saveMany($experiences);

        return back();
    }
}
