<?php

namespace Novedades52\Http\Controllers\Admin\Auth;

use Novedades52\Admin;
use Validator;
use Novedades52\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Guard by default for admin authentication
     *
     * @var string
     */
    protected $guard = 'admin';

    /**
     * Login view for admins
     *
     * @var string
     */
    protected $loginView = 'admin.auth.login';

    /**
     * Registration view for admins
     *
     * @var string
     */
    protected $registerView = 'admin.auth.register';

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'full_name' => 'required|max:100',
            'email'     => 'required|email|max:255|unique:admins',
            'password'  => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new admin instance after a valid registration.
     *
     * @param  array $data
     *
     * @return Admin
     */
    protected function create(array $data)
    {
        return Admin::create([
            'full_name' => $data['full_name'],
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
        ]);
    }
}
