<?php

namespace Novedades52\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Novedades52\Http\Requests;
use Novedades52\Http\Controllers\Controller;
use Novedades52\Post;

class PostController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Post::class, 'post');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return '<h1>List of posts</h1>';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return '<h1>Create a post [form]</h1>';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return '<h1>Store a post in the DB</h1>';
    }

    /**
     * Display the specified resource.
     *
     * @param  \Novedades52\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        dd($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Novedades52\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return "<h1>Update the post {$post->id} [form]</h1>";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Novedades52\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        return "<h1>Update the post {$post->id} in the DB</h1>";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Novedades52\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        return "<h1>Destroy the post {$post->id} in the DB</h1>";
    }
}
