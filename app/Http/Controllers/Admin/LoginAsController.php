<?php

namespace Novedades52\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Novedades52\Http\Controllers\Controller;
use Novedades52\Http\Requests;
use Novedades52\User;

class LoginAsController extends Controller
{

    public function getLoginAs()
    {
        $users = User::where(['role' => 'user'])->get();

        return view('admin.loginas', compact('users'));
    }

    public function loginAs(User $user)
    {
        if ($user->isAdmin()) {
            session()->flash('error', 'You cannot login as an admin');
            return redirect()->to('/admin/login-as');
        }

        session(['admin' => auth()->user()]);

        auth()->login($user);

        return redirect()->to('/');
    }

    public function loginAsBack()
    {
        if (session()->has('admin')) {
            auth()->login(session('admin'));
            session()->forget('admin');
        }

        return redirect()->to('/');
    }
}
