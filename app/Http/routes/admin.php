<?php

use Illuminate\Foundation\Http\Middleware\Authorize;
use Novedades52\Post;
use Novedades52\User;

Route::get('/dashboard', function () {
    return '<h1>Welcome to the admin panel</h1>';
});

Route::resource('/posts', 'PostController', ['parameters' => [
    'posts' => 'post',
]]);

Route::get('/login-as', 'LoginAsController@getLoginAs');

Route::get('/login-as/{user}', 'LoginAsController@loginAs');

Route::get('/login-as-back', 'LoginAsController@loginAsBack');

Route::get('/users', function () {
    $users = User::all();

    return view('admin.users', compact('users'));
});

Route::resource('users', 'UserController', [
    'parameters' => [
        'users' => 'user',
    ],
]);