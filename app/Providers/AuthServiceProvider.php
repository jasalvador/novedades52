<?php

namespace Novedades52\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Novedades52\Policies\PostPolicy;
use Novedades52\Post;
use Novedades52\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'Novedades52\Model' => 'Novedades52\Policies\ModelPolicy',
        Post::class => PostPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('admin', function (User $user) {
            if (session()->has('admin'))
                return true;

            return $user->role == 'admin';
        });

        $gate->define('view-posts', function (User $user) {
            return true;
        });
    }
}
