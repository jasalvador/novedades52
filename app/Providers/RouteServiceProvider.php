<?php

namespace Novedades52\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Novedades52\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);
        $this->mapAdminRoutes($router);
        $this->mapAdminAuthRoutes($router);
        $this->mapApiRoutes($router);
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace,
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes/web.php');
        });
    }

    protected function mapAdminRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace . '\Admin',
            'prefix' => '/admin',
            'middleware' => 'admin',
        ], function ($router) {
            require app_path('Http/routes/admin.php');
        });
    }

    protected function mapAdminAuthRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace . '\Admin',
            'prefix' => '/admin',
            'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes/admin_auth.php');
        });
    }

    protected function mapApiRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace . '\Api',
            'prefix' => '/api',
            'middleware' => 'api',
        ], function ($router) {
            require app_path('Http/routes/api.php');
        });
    }
}
