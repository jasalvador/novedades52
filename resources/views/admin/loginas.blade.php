@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{ Session::get('error') }}
                    </div>
                @endif

                <div class="panel panel-default">

                    <div class="panel-heading">Login as</div>

                    <div class="panel-body">
                        Login as users

                        <ul>
                            @foreach($users as $user)
                                <li>
                                    {{ $user->name }} - <a href="/admin/login-as/{{ $user->id }}">Login as</a>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
