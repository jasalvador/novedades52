@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Welcome to <strong>Styde</strong></div>

                    <img src="http://novedades52.laravel.app/laravel_logo.png" width="400" alt="Logo Laravel">
                    
                    <div class="panel-body">
                        Your Application's Landing Page.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
